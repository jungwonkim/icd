# Set this if system does not have OpenCL headers in standard include directory
# CL_HEADER_PATH := .

CL_HEADER_PATH := ${HOME}/opencl/opencl1.2
CC := icpc

CFLAGS += -I${CL_HEADER_PATH}
CFLAGS += -fPIC

OUTDIR := ./bin

.PHONY: clean default

default: ${OUTDIR}/libOpenCL.so 

test:
	${MAKE} -C test

${OUTDIR}/libOpenCL.so: ${OUTDIR}/libOpenCL.so.1
	cd ${OUTDIR} && ln -sf libOpenCL.so.1 libOpenCL.so

${OUTDIR}/libOpenCL.so.1: ${OUTDIR}/libOpenCL.so.1.0
	cd ${OUTDIR} && ln -sf libOpenCL.so.1.0 libOpenCL.so.1

${OUTDIR}/libOpenCL.so.1.0: ${OUTDIR}/libOpenCL.so.1.0.0
	cd ${OUTDIR} && ln -sf libOpenCL.so.1.0.0 libOpenCL.so.1.0

${OUTDIR}/libOpenCL.so.1.0.0: icd.h icd_dispatch.h

${OUTDIR}/libOpenCL.so.1.0.0: icd.c icd_dispatch.c icd_linux.c

${OUTDIR}/libOpenCL.so.1.0.0: icd_exports.map
	mkdir -p ${OUTDIR}
	${CC} ${CFLAGS} -shared -Wl,-soname,libOpenCL.so.1 \
		-Wl,--version-script,icd_exports.map \
		-ldl \
		-o $@ \
		icd.c icd_dispatch.c icd_linux.c

clean:
	rm -rf ${OUTDIR}
	
